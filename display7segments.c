#include "display7segments.h"

//  --a--
// |     |
// f     b
// |     |
//  --g--  
// |     |
// e     c
// |     |
//  --d--    
//         o
//          p

//76543210
//abcdefgp
//bagfedcp

// 0 - abcdef
// 1 - bc
// 2 - abdeg
// 3 - abcdg
// 4 - bcfg
// 5 - acdfg
// 6 - acdefg
// 7 - abc
// 8 - abcdefg
// 9 - abcdfg
// A - abcefg    1010
// B - cdefg     1011
// C - adef      1100
// D - bcdeg     1101
// E - adefg     1110
// F - aefg      1111


uint8_t digits[] = {
	0xFC,//0
	0x60,
	0xDA,
	0xF2,
	0x66,//4
	0xB6,
	0xBE,
	0xE0,
	0xFE,//8
	0xF6,
	0xEE,
	0x3E,
	0x9C,//C
	0x7A,
	0x9E,
	0x8E
};
#define PORTA_BIT_MASK 0x7E
#define PORTA_PART1 0x07
#define PORTA_PART3 0xE0
#define PORTF_BIT_MASK 0x30
#define PORTF_PART2 0x18

int8_t  DISPLAY7SEGMENTS_Initialize   (void) {
	RCC->AHBENR |=  RCC_AHBENR_GPIOAEN;           /* Enable GPIOA clock         */
  RCC->AHBENR |=  RCC_AHBENR_GPIOFEN;                  /* Enable GPIOC clock         */

  /* Configure LED (PC.0..3) pins as push-pull outputs, No pull-up, pull-down */
	/* Configure LED (PA.0..3) pins as push-pull outputs, No pull-up, pull-down */
  
  PLATFORM_SetGPIOPortPinAsPushPullOutputWithoutPullUpPullDown(GPIOA,1);
	PLATFORM_SetGPIOPortPinAsPushPullOutputWithoutPullUpPullDown(GPIOA,2);
	PLATFORM_SetGPIOPortPinAsPushPullOutputWithoutPullUpPullDown(GPIOA,3);
  PLATFORM_SetGPIOPortPinAsPushPullOutputWithoutPullUpPullDown(GPIOF,4);
	PLATFORM_SetGPIOPortPinAsPushPullOutputWithoutPullUpPullDown(GPIOF,5);
  PLATFORM_SetGPIOPortPinAsPushPullOutputWithoutPullUpPullDown(GPIOA,4);
	PLATFORM_SetGPIOPortPinAsPushPullOutputWithoutPullUpPullDown(GPIOA,5);
	PLATFORM_SetGPIOPortPinAsPushPullOutputWithoutPullUpPullDown(GPIOA,6); 
	PLATFORM_SetGPIOPortPinAsPushPullOutputWithoutPullUpPullDown(GPIOA,7);
	//Connect PA7 to ground for common anode
	PLATFORM_GPIOPortPinOutputValueClr(GPIOA, 7);
  return 0;
}
int8_t  DISPLAY7SEGMENTS_SetSegments(uint8_t bits) {
	//BSRR is used to set or reset bits of the ODR (Output Data Register)
	//31-16 bits of the BSSR reset the 15-0 bits of the ODR
	//15-0  bits of the BSSR set the coresponding bits of the ODR
	//PART1 has the first 3 lsbits
	//PART3 has the most significant 3 bits, so it gets shifted 2 pos to right
	//everything gets shifted 1 bit to the left to skip pin A0
	uint16_t porta = ((bits&PORTA_PART1) | ((bits&PORTA_PART3)>>2))<<1;
	//PART2 has bits 4 and 3 and their values get shifted to positions 5 and 4
	uint16_t portf = ((bits&PORTF_PART2)<<1);
	//now the flags for setting and resetting the specific bits are being built
	//the PORT MASKS are being used to avoid changing bits 
	GPIOA->BSRR |= (((~porta)&PORTA_BIT_MASK)<<16)|(porta&PORTA_BIT_MASK);
	GPIOF->BSRR |= (((~portf)&PORTF_BIT_MASK)<<16)|(portf&PORTF_BIT_MASK);
	return 0;
}
uint8_t DISPLAY7SEGMENTS_GetSegments     (void) {
	//to be implemented
	return 0;
}
int32_t DISPLAY7SEGMENTS_SetDigit       (uint8_t val) {
	DISPLAY7SEGMENTS_SetSegments(digits[val]);
	return 0;	
}
