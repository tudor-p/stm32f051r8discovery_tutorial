#ifndef __DISPLAY_7_SEGMENTS_H
#define __DISPLAY_7_SEGMENTS_H

#include "platform.h"

int8_t  DISPLAY7SEGMENTS_Initialize   (void);
int8_t  DISPLAY7SEGMENTS_SetSegments       (uint8_t val);
uint8_t DISPLAY7SEGMENTS_GetSegments     (void);
int32_t DISPLAY7SEGMENTS_SetDigit       (uint8_t val);

#endif
