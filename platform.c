#include "platform.h"

void PLATFORM_SetGPIOPortPinAsPushPullOutputWithoutPullUpPullDown(GPIO_TypeDef *port, uint8_t pinNo) {
  port->MODER   &= ~(3UL << 2*pinNo);
  port->MODER   |=  (1UL << 2*pinNo);
  port->OTYPER  &= ~(1UL <<   pinNo);
  port->OSPEEDR &= ~(3UL << 2*pinNo);
  port->OSPEEDR |=  (1UL << 2*pinNo);
  port->PUPDR   &= ~(3UL << 2*pinNo);
}

void PLATFORM_MakeGPIOPortPinAsPushPullOutput(GPIO_TypeDef *port, uint8_t pinNo, _Bool initial_state) {
	if(initial_state) {
		port->BSRR |= (1<<pinNo);
	} else {
		port->BSRR |= ((1<<pinNo)<<16);
	}  
	port->MODER   &= ~(3UL << 2*pinNo);
  port->MODER   |=  (1UL << 2*pinNo);
  port->OTYPER  &= ~(1UL <<   pinNo);
  port->OSPEEDR &= ~(3UL << 2*pinNo);
  port->OSPEEDR |=  (1UL << 2*pinNo);
  port->PUPDR   &= ~(3UL << 2*pinNo);
}


void PLATFORM_MakeGPIOPortPinInputNoPullUpOrDown(GPIO_TypeDef *port, uint8_t pinNo) {
	port->MODER    &= ~((3UL << 2*pinNo)  );         /* input              */
  port->OSPEEDR  &= ~((3UL << 2*pinNo)  );         /* Low Speed          */
  port->PUPDR    &= ~((3UL << 2*pinNo)  );         /* no Pull up         */
}

void PLATFORM_MakeGPIOPortPinInputWithPullDown(GPIO_TypeDef *port, uint8_t pinNo) {
	port->MODER    &= ~((3UL << 2*pinNo)  );         /* input              */
  port->OSPEEDR  &= ~((3UL << 2*pinNo)  );         /* Low Speed          */
  port->PUPDR    &= ~((3UL << 2*pinNo)  );         /* Pull down         */
	port->PUPDR    |= ((2UL << 2*pinNo)  );         /* Pull down         */
}

void PLATFORM_MakeGPIOPortPinInputWithPullUp(GPIO_TypeDef *port, uint8_t pinNo) {
	port->MODER    &= ~((3UL << 2*pinNo)  );         /* input              */
  port->OSPEEDR  &= ~((3UL << 2*pinNo)  );         /* Low Speed          */
  port->PUPDR    &= ~((3UL << 2*pinNo)  );         /* Pull down         */
	port->PUPDR    |= ((1UL << 2*pinNo)  );         /* Pull down         */
}

void PLATFORM_GPIOPortPinOutputValueSet(GPIO_TypeDef *port, uint8_t pinNo) {
	port->BSRR |= (1<<pinNo);
}

void PLATFORM_GPIOPortPinOutputValueClr(GPIO_TypeDef *port, uint8_t pinNo) {
	port->BSRR |= ((1<<pinNo)<<16);
}

_Bool PLATFORM_GetGPIOPortPinInputState(GPIO_TypeDef *port, uint8_t pinNo) {
	return ((port->IDR & (1UL << pinNo)) != 0);
}

_Bool PLATFORM_GetGPIOPinState(PLATFORM_Pin *pin) {
	return ((pin->port->IDR & (1UL << pin->pinNo)) != 0);
}
