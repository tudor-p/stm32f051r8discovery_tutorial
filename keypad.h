#ifndef __KEYPAD_H__
#define __KEYPAD_H__

#include "platform.h"

#define KEYPAD_RESULT_NO_KEY_PRESSED -1
#define KEYPAD_RESULT_MULTIPLE_KEYS_PRESSED -2

void KEYPAD_Initialize(void);
int8_t KEYPAD_ReadKey(void);

#endif
