#ifndef PLATFORM_H__
#define PLATFORM_H__
#include "stm32f0xx.h"                  // Device header
#include <stdint.h>

typedef struct _PLATFORM_Pin {
	GPIO_TypeDef *port;
	uint8_t pinNo;
} PLATFORM_Pin;


void PLATFORM_SetGPIOPortPinAsPushPullOutputWithoutPullUpPullDown(GPIO_TypeDef *port, uint8_t pinNo);
void PLATFORM_MakeGPIOPortPinAsPushPullOutput(GPIO_TypeDef *port, uint8_t pinNo, _Bool initial_state);
void PLATFORM_MakeGPIOPortPinInputNoPullUpOrDown(GPIO_TypeDef *port, uint8_t pinNo);
void PLATFORM_MakeGPIOPortPinInputWithPullDown(GPIO_TypeDef *port, uint8_t pinNo);
void PLATFORM_MakeGPIOPortPinInputWithPullUp(GPIO_TypeDef *port, uint8_t pinNo);
void PLATFORM_GPIOPortPinOutputValueSet(GPIO_TypeDef *port, uint8_t pinNo);
void PLATFORM_GPIOPortPinOutputValueClr(GPIO_TypeDef *port, uint8_t pinNo);
_Bool PLATFORM_GetGPIOPortPinInputState(GPIO_TypeDef *port, uint8_t pinNo);
_Bool PLATFORM_GetGPIOPinState(PLATFORM_Pin *pin);

#endif
