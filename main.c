#include "stm32f0xx.h"
#include "Board_LED.h"
#include "Board_Buttons.h"
#include "display7segments.h"
#include "keypad.h"

#define LED_BLUE 0
#define LED_GREEN 1

uint32_t msTicks = 0; //milisecond tick counter

void BlinkLed() {
	//count a new passed ms
	if(((msTicks / 500) & 0x1) == 0) {//keep blue led on 500ms and off 500ms
		LED_On(LED_BLUE);
	} else {
		LED_Off(LED_BLUE);
	}
}

void SysTick_Handler(void) { // SysTick interrupt Handler.
	msTicks++;
}


void prepareSysTick() {
	uint32_t returnCode;
	returnCode = SysTick_Config(SystemCoreClock / 1000); //interrupt every 1ms
	if (returnCode != 0) { //verify error code
		// Error Handling
	}
}


int main() {
	uint8_t state = 0;
	uint32_t new_state_since = 0;
	uint8_t count = 0;
	
	SystemInit();
	
	LED_Initialize();
	LED_On(LED_BLUE);
	LED_On(LED_GREEN);
	
	Buttons_Initialize();
	
	DISPLAY7SEGMENTS_Initialize();
	
	prepareSysTick();
	
	KEYPAD_Initialize();
	
	while(1) {
		int8_t key;
		uint8_t new_state = Buttons_GetState();		
		
		if(state) {
			LED_On(LED_GREEN);
		} else {
			LED_Off(LED_GREEN);
		}
		
		if(state == new_state) {
			new_state_since = msTicks;
		} else {
			//transition to a new button state only after 100ms
			if((msTicks-new_state_since)>100) {
				state = new_state;
				new_state_since = msTicks;
				count++;
				count&=0x0F;
				if(count==0) {
					LED_On(LED_BLUE);
				} else {
					LED_Off(LED_BLUE);
				}
				DISPLAY7SEGMENTS_SetDigit(count);
			}
		}
		
		key = KEYPAD_ReadKey();
		
		if(key>KEYPAD_RESULT_NO_KEY_PRESSED) {
			DISPLAY7SEGMENTS_SetDigit(key-'0');
		}
	}
}
