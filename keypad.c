#include "keypad.h"

#define COLUMNS 3
#define ROWS 4

const int8_t keyMap[ROWS][COLUMNS] = {
	{ '1', '2', '3' },
	{ '4', '5', '6' },
	{ '7', '8', '9' },
	{ '0'+10, '0', '0'+11 },
};

PLATFORM_Pin columns[COLUMNS], rows[ROWS];

//Sets the pins associated with each of the columns as outputs with high voltage level
//helps to easily detect if there is any key pressed
void ActivateAllColumns() {
	uint8_t i;
	for(i=0;i<COLUMNS;++i) {
		PLATFORM_MakeGPIOPortPinAsPushPullOutput(columns[i].port, columns[i].pinNo, 1);
	}
}

//Initializes the pins used to drive keypad column lines
//  and the pins used to read the keypad row lines
void KEYPAD_Initialize(void) {
	uint8_t i;

	//assign column gpios and pin numbers
	columns[0].port = GPIOB;	
	columns[0].pinNo = 5;
	columns[1].port = GPIOB;
	columns[1].pinNo = 7;
	columns[2].port = GPIOB;
	columns[2].pinNo = 3;
	
	rows[0].port = GPIOB;
	rows[0].pinNo = 6;
	rows[1].port = GPIOC;
	rows[1].pinNo = 12;
	rows[2].port = GPIOD;
	rows[2].pinNo = 2;
	rows[3].port = GPIOB;
	rows[3].pinNo = 4;
	
	
	//enabling clocks for the used peripherals
	RCC->AHBENR |=  RCC_AHBENR_GPIOBEN;           /* Enable GPIOB clock*/
  RCC->AHBENR |=  RCC_AHBENR_GPIOCEN;           /* Enable GPIOC clock*/
	RCC->AHBENR |=  RCC_AHBENR_GPIODEN;           /* Enable GPIOD clock*/
	
	//make rows inputs with pull downs
	for(i=0;i<ROWS;++i) {
		PLATFORM_MakeGPIOPortPinInputWithPullDown(rows[i].port, rows[i].pinNo);
	}	
	
	//make the columns outputs with high state
	ActivateAllColumns();
}

//Activates only the selected column by making it output with high voltage level
//  and deactivates the rest by making them inputs with pull downs
// Allows detecting which of the individual keys of the selected column is pressed
void ActivateOnlyColumn(uint8_t col) {
	uint8_t i;
	for(i=0;i<COLUMNS;++i) {
		if(i==col) {
			PLATFORM_MakeGPIOPortPinAsPushPullOutput(columns[i].port, columns[i].pinNo, 1);
		} else {
			PLATFORM_MakeGPIOPortPinInputWithPullDown(columns[i].port, columns[i].pinNo);
		}
	}
}

//Reads the state of each row and returns:
//  * its number
//  * KEYPAD_RESULT_NO_KEY_PRESSED (if no active row was detected)
//  * KEYPAD_RESULT_MULTIPLE_KEYS_PRESSED (if multiple rows are selected)
int8_t DetectSelectedRow() {
	int8_t result = KEYPAD_RESULT_NO_KEY_PRESSED; //in case there is no key pressed the result remains unchanged
	uint8_t i;
	for(i=0;i<ROWS;++i) {
		if(PLATFORM_GetGPIOPinState(rows+i)) {
			if(result!=KEYPAD_RESULT_NO_KEY_PRESSED) {
				//this is the second active row since we already had a result
				return KEYPAD_RESULT_MULTIPLE_KEYS_PRESSED;
			}
			result = i;
		}		
	}
	return result;
}

//Activates each of the columns, one by one, one at the time and scans the rows for pressed keys
int8_t ScanColumnsForKey() {
	int8_t result = KEYPAD_RESULT_NO_KEY_PRESSED;
	int8_t activeRow;
	uint8_t i;
	for(i=0;i<COLUMNS;++i) {
		ActivateOnlyColumn(i);
		activeRow = DetectSelectedRow();
		if(activeRow < KEYPAD_RESULT_NO_KEY_PRESSED) {
			//some error code was detected
			return activeRow;
		} 
		if(activeRow == KEYPAD_RESULT_NO_KEY_PRESSED) {
			//no row activated by current column... go to next column
			continue;
		} 
		if(result != KEYPAD_RESULT_NO_KEY_PRESSED) {
			//there was already a key detected
			return KEYPAD_RESULT_MULTIPLE_KEYS_PRESSED;
		}
		result = keyMap[activeRow][i];
	}
	return result;
}
//Reads the state of the keypad and returns
//  * key code decoded using keyMap mapping matrix
//  * KEYPAD_RESULT_NO_KEY_PRESSED (if no active row was detected)
//  * KEYPAD_RESULT_MULTIPLE_KEYS_PRESSED (if multiple rows are selected)
int8_t KEYPAD_ReadKey(void) {
	int8_t activeRow;
	ActivateAllColumns();
	activeRow = DetectSelectedRow();
	if(activeRow <= KEYPAD_RESULT_NO_KEY_PRESSED) {
		//no key pressed or other error detected, just return the error code
		return activeRow;
	} else {
		//at least one key is pressed, but needs column scaning to identify it
		return ScanColumnsForKey();
	}
}
