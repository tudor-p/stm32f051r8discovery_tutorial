# Tutorial for Discovery development Boad based on ARM6-M Cortex-M0 STM32f051R8 micro-controller #

## About ##

The project illustrates with examples how to:

* configure a Microvision project for working with the Discovery development board
* use the LEDs
* use the user button
* use a 7 segment digit display
* use a 4x3 keypad

## Resources ##

In the download section it can be found a tutorial document in pdf format describing the steps needed to configure the Microvision (uVision) environment. The bibliography of the tutorial contains useful links to the datasheets and the reference programming manual for the micro-controller family.

For the moment the tutorial text is written only in Romanian language, but the screen captures should be more than useful for crossing over the language gap.

## Development steps ##

The development steps can be accessed using the Downloads section under the second subsection (Tags). The tagged development steps (commits) can be downloaded as individual packages (without needing the Git version control software) and used as starting point for your own endeavors.